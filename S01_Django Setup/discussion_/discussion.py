# django-admin startproject S01
# python manage.py runserver


# S02 Django MVC - Models Views and Controllers ----------------------

# model - DRY principle

# python manage.py makemigrations todolist - to apply
# python manage.py sqlmigrate todolist 0001
# python manage.py sqlmigrate django_practice 0001
# python manage.py migrate


# python manage.py shell -------------
# from todolist.models import ToDoItem;
# ToDoItem.objects.all()
# from django.utils import timezone;
# todoitem = ToDoItem(task_name="Eat", description="Dinner time, order pizza with extra cheese", date_created=timezone.now())
# todoitem.save()
# todoitem.id
# todoitem.status
# todoitem.status = "completed"

# todoitem = ToDoItem.objects.filter(id=1)
# todoitem = [0]

# todoitem[0].task_name
# todoitem[0].description

# exit()

# pip install mysql_connector
# pip install mysql

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'python_db',
#         'USER': 'root',
#         'PASSWORD': '',
#         'HOST': '127.0.0.1',
#         'PORT': '3306'
#     }
# }



# python manage.py makemigrations todolist

# from todolist.models import ToDoItem;


# super user
# winpty python manage.py createsuperuser

# python manage.py runserver



# 03_Django User Authentication ------------------------

# Django User Objects

# S04 - Template inheritance -----------------------

# Django forms and CRUD Operations

# Appying bootstrap