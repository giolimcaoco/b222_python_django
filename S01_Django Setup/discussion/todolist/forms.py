from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class LoginForm(forms.Form):
	username = forms.CharField(label='Username', max_length=20)
	password = forms.CharField(label="Password", max_length=20)

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)

class AddEventForm(forms.Form):
	event_name = forms.CharField(label="Event Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)
	event_date = forms.CharField(label="Event Date",max_length=50)
	

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label='Task Name', max_length=50)
	description = forms.CharField(label="Description", max_length=200)
	status = forms.CharField(label="status", max_length=50)

class RegisterForm(forms.Form):
	username = forms.CharField(label='User Name', max_length=50)
	first_name = forms.CharField(label='First Name', max_length=50)
	last_name = forms.CharField(label='Last Name', max_length=50)
	email = forms.CharField(label='Email', max_length=50)
	password = forms.CharField(label='Password', max_length=50)
	# confirm_password = forms.CharField(label="Confirm Password", max_length=50)

	# def clean_password_confirm(self):
	#         cleaned_data = super().clean()
	#         if cleaned_data.get('password') != cleaned_data.get('password_confirm'):
	#             raise ValidationError('Password fields do not match.')

class UpdateProfile(forms.Form):
	first_name = forms.CharField(label="First Name", max_length=50)
	last_name = forms.CharField(label="last_name", max_length=50)
	password = forms.CharField(label="Password", max_length=50)